package com.jiyun;

import com.jiyun.util.JwtUtil;
import io.jsonwebtoken.Claims;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MyTest {

    @Autowired
    RedisTemplate<String,Object> redisTemplate;//用来操作redis的

    @Autowired
    JwtUtil jwtUtil;

    @Test
    public void test1(){
//        redisTemplate.boundValueOps("name").set("aaa");
//
//        System.out.println(redisTemplate.boundValueOps("name").get());

//        redisTemplate.delete("name");





        String token = "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VySW5mbyI6eyJpZCI6MSwidXNlcm5hbWUiOiJhZG1pbiIsInBhc3N3b3JkIjoiMTIzIiwibG9naW5OYW1lIjoi6ams5LqRNjY2IiwicGhvbmVudW0iOm51bGwsImlzbWFuYWdlciI6IjEiLCJpc3N5c3RlbSI6IjEiLCJzdGF0dXMiOiIwIiwidGVuYW50SWQiOjF9LCJtZW51TGlzdCI6W3siaWQiOjEsIm1uYW1lIjoi57O757uf566h55CGNjY2IiwidXJsIjoiL3N5c3RlbS9saXN0IiwicGlkIjpudWxsLCJpc2RlbGV0ZSI6MX0seyJpZCI6MiwibW5hbWUiOiLml6Xlv5fnrqHnkIYiLCJ1cmwiOiIvbG9nL2xpc3QiLCJwaWQiOm51bGwsImlzZGVsZXRlIjoxfSx7ImlkIjozLCJtbmFtZSI6IumUgOWUruiusOW9lSIsInVybCI6Ii9yZWNvcmQvbGlzdCIsInBpZCI6bnVsbCwiaXNkZWxldGUiOjF9XSwicm9sZUxpc3QiOlt7ImlkIjoxLCJybmFtZSI6IuiAgeadvzY2NiIsImlzZGVsZXRlIjoxfV0sImV4cCI6MTY3NTc1NDI0NCwiaWF0IjoxNjc1NDk1MDQ0fQ.4oERMuyGjhxdqav7mGF2b6AvcblstBvqGVRr-_WcJ7Y";


        Claims claims = jwtUtil.parseJWT(token);
        Date expiration = claims.getExpiration();

//        JiyunUser userInfo = claims.get("userInfo", JiyunUser.class);
        LinkedHashMap userInfo = (LinkedHashMap)claims.get("userInfo");
        List roleList = claims.get("roleList", List.class);


        System.out.println("---------------------------");
        System.out.println(expiration);
        System.out.println(userInfo);
        System.out.println(roleList);
        System.out.println("---------------------------");
    }


}
