package com.jiyun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class JiyunUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String username;

    private String password;

    private String loginName;

    private String phonenum;

    private String ismanager;

    private String issystem;

    private String status;

    private Integer tenantId;


}
