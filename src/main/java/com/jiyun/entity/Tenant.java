package com.jiyun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Tenant implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String name;

    private Integer userNumLimit;

    private String type;

    private String enable;

    private Date createTime;

    private Date expireTime;

    private String remark;


}
