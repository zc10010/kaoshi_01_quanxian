package com.jiyun.dto;

import com.jiyun.entity.JiyunUser;
import lombok.Data;

@Data
public class JiyunUserDto extends JiyunUser {

    private String tenantName;
}
