package com.jiyun.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.jiyun.entity.UserRole;
import com.jiyun.service.IUserRoleService;
import java.util.List;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-02
 */
@RestController
@RequestMapping("/user-role")
public class UserRoleController {

    @Autowired
    IUserRoleService userRoleService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addUserRole(@RequestBody UserRole userRole){
        userRoleService.save(userRole);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateUserRole(@RequestBody UserRole userRole){
        userRoleService.updateById(userRole);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        userRoleService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        userRoleService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public UserRole findById(@RequestParam Integer id){
        return userRoleService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<UserRole> findAll(){
        return userRoleService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<UserRole> findPage(@RequestParam Integer page,@RequestParam Integer pageSize){
        Page<UserRole> userRolePage = new Page<>(page, pageSize);
        return userRoleService.page(userRolePage);
    }

}
