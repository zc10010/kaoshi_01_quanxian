package com.jiyun.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.Menu;
import com.jiyun.entity.RoleMenu;
import com.jiyun.service.IMenuService;
import com.jiyun.service.IRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-02
 */
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    IMenuService menuService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addMenu(@RequestBody Menu menu){
        menuService.save(menu);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateMenu(@RequestBody Menu menu){
        menuService.updateById(menu);
    }


    @Autowired
    IRoleMenuService roleMenuService;
    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public String deleteOne(@RequestParam Integer id){


        LambdaQueryWrapper<RoleMenu> roleMenuLambdaQueryWrapper = new LambdaQueryWrapper<>();

        roleMenuLambdaQueryWrapper.eq(RoleMenu::getMid,id);

        int count = roleMenuService.count(roleMenuLambdaQueryWrapper);
        if(count>0){
            return "有关联数据，删除失败";
        }
        menuService.removeById(id);
        return "删除成功";
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        menuService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public Menu findById(@RequestParam Integer id){
        return menuService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<Menu> findAll(){
        return menuService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<Menu> findPage(@RequestParam Integer page, @RequestParam Integer pageSize,@RequestBody Menu menu){
        LambdaQueryWrapper<Menu> menuLambdaQueryWrapper = new LambdaQueryWrapper<>();
        menuLambdaQueryWrapper.like(StringUtils.isNotBlank(menu.getMname()),Menu::getMname,menu.getMname());

        Page<Menu> menuPage = new Page<>(page, pageSize);
        return menuService.page(menuPage,menuLambdaQueryWrapper);
    }

}
