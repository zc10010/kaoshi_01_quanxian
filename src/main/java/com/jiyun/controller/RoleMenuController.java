package com.jiyun.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.jiyun.entity.RoleMenu;
import com.jiyun.service.IRoleMenuService;
import java.util.List;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-02
 */
@RestController
@RequestMapping("/role-menu")
public class RoleMenuController {

    @Autowired
    IRoleMenuService roleMenuService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addRoleMenu(@RequestBody RoleMenu roleMenu){
        roleMenuService.save(roleMenu);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateRoleMenu(@RequestBody RoleMenu roleMenu){
        roleMenuService.updateById(roleMenu);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        roleMenuService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        roleMenuService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public RoleMenu findById(@RequestParam Integer id){
        return roleMenuService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<RoleMenu> findAll(){
        return roleMenuService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<RoleMenu> findPage(@RequestParam Integer page,@RequestParam Integer pageSize){
        Page<RoleMenu> roleMenuPage = new Page<>(page, pageSize);
        return roleMenuService.page(roleMenuPage);
    }

}
