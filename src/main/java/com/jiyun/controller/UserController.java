package com.jiyun.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.User;
import com.jiyun.entity.UserRole;
import com.jiyun.service.IUserRoleService;
import com.jiyun.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-02
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    IUserService userService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addUser(@RequestBody User user){
        userService.save(user);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateUser(@RequestBody User user){
        userService.updateById(user);
    }


    @Autowired
    IUserRoleService userRoleService;
    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        //删除用户基本信息
        userService.removeById(id);
        //删除中间表信息
        LambdaQueryWrapper<UserRole> userRoleLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userRoleLambdaQueryWrapper.eq(UserRole::getUid,id);
        userRoleService.remove(userRoleLambdaQueryWrapper);

    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        userService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public User findById(@RequestParam Integer id){
        return userService.getById(id);
    }


    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findUserRole")
    public List<UserRole> findUserRole(@RequestParam Integer id){
        LambdaQueryWrapper<UserRole> userRoleLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userRoleLambdaQueryWrapper.eq(UserRole::getUid,id);
        List<UserRole> userRoleList = userRoleService.list(userRoleLambdaQueryWrapper);
        return userRoleList;
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/updateUserRole")
    public void updateUserRole(@RequestParam Integer uid,@RequestParam String rids){
        LambdaQueryWrapper<UserRole> userRoleLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userRoleLambdaQueryWrapper.eq(UserRole::getUid,uid);

        userRoleService.remove(userRoleLambdaQueryWrapper);

        String[] ridsArr = rids.split(",");
        for (String rid : ridsArr) {
            UserRole userRole = new UserRole();
            userRole.setUid(uid);
            userRole.setRid(Integer.parseInt(rid));
            userRoleService.save(userRole);
        }


    }




    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<User> findAll(){
        return userService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<User> findPage(@RequestParam Integer page,@RequestParam Integer pageSize,@RequestBody User user){


        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();

        userLambdaQueryWrapper.and(StringUtils.isNotBlank(user.getLoginName()), q->{
            q.like(User::getLoginName,user.getLoginName()).or().like(User::getUsername,user.getLoginName());
        });

        Page<User> userPage = new Page<>(page, pageSize);
        return userService.page(userPage,userLambdaQueryWrapper);
    }

}
