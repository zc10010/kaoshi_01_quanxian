package com.jiyun.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.dto.JiyunUserDto;
import com.jiyun.entity.JiyunUser;
import com.jiyun.entity.UserRole;
import com.jiyun.service.IJiyunUserService;
import com.jiyun.service.IUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-03
 */
@RestController
@RequestMapping("/jiyun-user")
public class JiyunUserController {

    @Autowired
    IJiyunUserService jiyunUserService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addJiyunUser(@RequestBody JiyunUser jiyunUser){
        jiyunUser.setStatus("2");
        jiyunUserService.save(jiyunUser);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateJiyunUser(@RequestBody JiyunUser jiyunUser){
        jiyunUserService.updateById(jiyunUser);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){


        JiyunUser jiyunUser = new JiyunUser();
        jiyunUser.setId(id);
        jiyunUser.setStatus("1");
        jiyunUserService.updateById(jiyunUser);


    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        jiyunUserService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public JiyunUser findById(@RequestParam Integer id){
        return jiyunUserService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<JiyunUser> findAll(){
        return jiyunUserService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<JiyunUserDto> findPage(@RequestParam Integer page, @RequestParam Integer pageSize, @RequestBody JiyunUser jiyunUser){
        return jiyunUserService.findPage(page,pageSize,jiyunUser);
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPageAdmin")
    public Page<JiyunUserDto> findPageAdmin(@RequestParam Integer page, @RequestParam Integer pageSize, @RequestBody JiyunUser jiyunUser){
        return jiyunUserService.findPageAdmin(page,pageSize,jiyunUser);
    }


    @Autowired
    IUserRoleService userRoleService;

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/updateUserRoleAdmin")
    public void updateUserRoleAdmin(@RequestParam Integer uid,@RequestParam String rids){
        //需要先更新用户状态为0
        JiyunUser jiyunUser = new JiyunUser();
        jiyunUser.setId(uid);
        jiyunUser.setStatus("0");
        jiyunUserService.updateById(jiyunUser);



        LambdaQueryWrapper<UserRole> userRoleLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userRoleLambdaQueryWrapper.eq(UserRole::getUid,uid);

        userRoleService.remove(userRoleLambdaQueryWrapper);

        String[] ridsArr = rids.split(",");
        for (String rid : ridsArr) {
            UserRole userRole = new UserRole();
            userRole.setUid(uid);
            userRole.setRid(Integer.parseInt(rid));
            userRoleService.save(userRole);
        }


    }

}
