package com.jiyun.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.Tenant;
import com.jiyun.service.ITenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-03
 */
@RestController
@RequestMapping("/tenant")
public class TenantController {

    @Autowired
    ITenantService tenantService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addTenant(@RequestBody Tenant tenant){
        tenantService.save(tenant);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateTenant(@RequestBody Tenant tenant){
        tenantService.updateById(tenant);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        tenantService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        tenantService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public Tenant findById(@RequestParam Integer id){
        return tenantService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<Tenant> findAll(){
        return tenantService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<Tenant> findPage(@RequestParam Integer page,@RequestParam Integer pageSize,@RequestBody Tenant tenant){

        LambdaQueryWrapper<Tenant> tenantLambdaQueryWrapper = new LambdaQueryWrapper<>();
        tenantLambdaQueryWrapper.like(StringUtils.isNotBlank(tenant.getName()), Tenant::getName,tenant.getName());

        Page<Tenant> tenantPage = new Page<>(page, pageSize);
        return tenantService.page(tenantPage,tenantLambdaQueryWrapper);
    }

}
