package com.jiyun.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.Role;
import com.jiyun.entity.RoleMenu;
import com.jiyun.entity.UserRole;
import com.jiyun.service.IRoleMenuService;
import com.jiyun.service.IRoleService;
import com.jiyun.service.IUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-02
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    IRoleService roleService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addRole(@RequestBody Role role){
        roleService.save(role);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateRole(@RequestBody Role role){
        roleService.updateById(role);
    }


    @Autowired
    IUserRoleService userRoleService;
    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public String deleteOne(@RequestParam Integer id){


        LambdaQueryWrapper<UserRole> userRoleLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userRoleLambdaQueryWrapper.eq(UserRole::getRid,id);

        int count = userRoleService.count(userRoleLambdaQueryWrapper);

        if(count>0){
            return "有关联数据，删除失败";
        }
        roleService.removeById(id);
        return "删除成功";


    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        roleService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public Role findById(@RequestParam Integer id){
        return roleService.getById(id);
    }



    @Autowired
    IRoleMenuService roleMenuService;

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findRoleMenu")
    public List<RoleMenu> findRoleMenu(@RequestParam Integer id){
        LambdaQueryWrapper<RoleMenu> roleMenuLambdaQueryWrapper = new LambdaQueryWrapper<>();
        roleMenuLambdaQueryWrapper.eq(RoleMenu::getRid,id);
        return roleMenuService.list(roleMenuLambdaQueryWrapper);
    }


    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/updateRoleMenu")
    public void updateRoleMenu(@RequestParam Integer rid,@RequestParam String mids){
        LambdaQueryWrapper<RoleMenu> roleMenuLambdaQueryWrapper = new LambdaQueryWrapper<>();
        roleMenuLambdaQueryWrapper.eq(RoleMenu::getRid,rid);
        roleMenuService.remove(roleMenuLambdaQueryWrapper);

        for (String mid : mids.split(",")) {
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setRid(rid);
            roleMenu.setMid(Integer.parseInt(mid));
            roleMenuService.save(roleMenu);
        }
    }
    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<Role> findAll(){
        return roleService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<Role> findPage(@RequestParam Integer page,@RequestParam Integer pageSize,@RequestBody Role role){

        LambdaQueryWrapper<Role> roleLambdaQueryWrapper = new LambdaQueryWrapper<>();

        roleLambdaQueryWrapper.like(StringUtils.isNotBlank(role.getRname()),Role::getRname,role.getRname());

        Page<Role> rolePage = new Page<>(page, pageSize);
        return roleService.page(rolePage,roleLambdaQueryWrapper);
    }

}
