package com.jiyun.mapper;

import com.jiyun.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-02
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
