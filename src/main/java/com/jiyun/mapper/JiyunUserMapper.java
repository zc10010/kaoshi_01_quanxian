package com.jiyun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.dto.JiyunUserDto;
import com.jiyun.entity.JiyunUser;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-03
 */
public interface JiyunUserMapper extends BaseMapper<JiyunUser> {

    Page<JiyunUserDto> findPage(Page<JiyunUserDto> jiyunUserPage,  @Param("user") JiyunUser jiyunUser);

    Page<JiyunUserDto> findPageAdmin(Page<JiyunUserDto> jiyunUserPage,  @Param("user") JiyunUser jiyunUser);

}
