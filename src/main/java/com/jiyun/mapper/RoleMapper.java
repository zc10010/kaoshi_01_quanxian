package com.jiyun.mapper;

import com.jiyun.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-02
 */
public interface RoleMapper extends BaseMapper<Role> {

}
