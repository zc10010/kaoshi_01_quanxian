package com.jiyun.mapper;

import com.jiyun.entity.Tenant;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-03
 */
public interface TenantMapper extends BaseMapper<Tenant> {

}
