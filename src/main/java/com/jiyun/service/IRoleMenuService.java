package com.jiyun.service;

import com.jiyun.entity.RoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-02
 */
public interface IRoleMenuService extends IService<RoleMenu> {

}
