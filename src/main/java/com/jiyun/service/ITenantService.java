package com.jiyun.service;

import com.jiyun.entity.Tenant;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-03
 */
public interface ITenantService extends IService<Tenant> {

}
