package com.jiyun.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jiyun.dto.JiyunUserDto;
import com.jiyun.entity.JiyunUser;
import com.jiyun.mapper.JiyunUserMapper;
import com.jiyun.service.IJiyunUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-03
 */
@Service
public class JiyunUserServiceImpl extends ServiceImpl<JiyunUserMapper, JiyunUser> implements IJiyunUserService {


    @Autowired(required = false)
    JiyunUserMapper jiyunUserMapper;

    @Override
    public Page<JiyunUserDto> findPage(Integer page, Integer pageSize, JiyunUser jiyunUser) {
        Page<JiyunUserDto> jiyunUserPage = new Page<>(page, pageSize);
        jiyunUserMapper.findPage(jiyunUserPage,jiyunUser);
        return jiyunUserPage;
    }

    @Override
    public Page<JiyunUserDto> findPageAdmin(Integer page, Integer pageSize, JiyunUser jiyunUser) {
        Page<JiyunUserDto> jiyunUserPage = new Page<>(page, pageSize);
        jiyunUserMapper.findPageAdmin(jiyunUserPage,jiyunUser);
        return jiyunUserPage;
    }
}
