package com.jiyun.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jiyun.dto.JiyunUserDto;
import com.jiyun.entity.JiyunUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-02-03
 */
public interface IJiyunUserService extends IService<JiyunUser> {

    Page<JiyunUserDto> findPage(Integer page, Integer pageSize, JiyunUser jiyunUser);
    Page<JiyunUserDto> findPageAdmin(Integer page, Integer pageSize, JiyunUser jiyunUser);
}
